# Project 1 - Chain Reaction

Name: Gerard Murphy 
Email: 18204937@studentmail.ul.ie

## Task 2 - Initial AI

### Strategy - SecondAI
* Prioritize our cells that are endangered
* If not endangered, avoid exploding critical cells
* Choose Corners over edges
* Choose Edges over Middle

