## Project 1 - Chain Reaction

1. Task 1 - Get started in DIME and Chain Reaction 
2. Post - Upload a screen shot of the DIME application (Chain Reaction) to the forum (due Feb 29th)
3. Task 2- Build a first Chain Reaction AI/strategy using the DSL provided 
4. Submit - Share your strategy to GitLab and post in the project 1 forum a link to your strategy (worth 10%) (due Mar 4th)

**Student Name:** Gerry Carty

**Student Id:** 18217249

**Strategy:**  Instant Gratification.  Choose cell that provides the biggst increase in the number of atoms in next placement.
Basically I count how many atoms my AI player has before it performs its next placement.  Then count how many atoms it would have after each possible placement.  Subtract the after from the before count to give the score.  The cell with the highest score is selected.  It's good for beating up the opponent in the short term.  Might be lacking in long term strategic planning.

 * NewAI9.process - High level process
 * MostAtoms.process - Count the number of atoms AI player would have after next placement.