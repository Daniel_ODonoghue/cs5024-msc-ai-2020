# CS5024 - Theory & Practice of Advanced AI Ecosystems(TPAAIE)

## Project 1

**Student Name:** Abhijit Sinha

**Student ID:** 18195067

**Student Email:** 18195067@studentmail.ul.ie

---

Task 2 file description:

| Filename                       | Description                             |
|--------------------------------|-----------------------------------------|
| `IsCornerOrEdgeCell.process`   | The Sub AI to detect edges and corners  |
| `CR_AI_Task2.process`          | The Top Level hierachical AI            |
