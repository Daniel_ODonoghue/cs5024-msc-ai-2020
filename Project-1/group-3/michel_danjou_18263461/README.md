#### Michel Danjou
#### Student id: 18263461
#### Msc AI - CS 5024 - Project 1, task 2

# Chain reaction
## Files
  * *Strategy*: This is the main strategy file. 
  The aim is to favor corners first, edges second, and then prioritize critical cells that are endangered.
  I then simulate a move using GetCopy/PlaceAtom/ReactField and call CountPlayerCells which I then add to the score.
   
  * *subSub_take5*: this is the process described on slide "Hierachy - The Sub AI"
    * please note that I couldn't rename the Success node to Middle as suggested in the PDF as it caused an error in the `sub sib` Java code.
     
  * I was able to play this heuristic, and have fun with it. I can beat it.
  * I have ran a tournament between `Strategy` and `Standard AI`. Unfortunaltely `Strategy` lost 99% of the games. 
  Needless to say there is room for improvements. 

  
**Note:**

I have add multiple issues with the Eclipse application. See forum.
I am posting something very basic because this is all I can get to work at present.
