# CS5024 Project 1 - Chain Reaction AI

## Seamus Brady-18195148

I thought I would try and wire in some randomness to make it more amusing.

Hence, the AI is called NyxBot, after the the Greek goddess of the Night, daughter of Chaos.

Perhaps such a modest AI does not warrant such a fancy name, but it sounds cool :)

##  AI Strategy

If you cannot beat 'em by being clever, at least be unpredictable.


##  AI Process

* Checks to see if atom placement is possible. 
  * --> If true, adds 250. 
  * --> If false, goto next step.
* Checks to see if this is an edge or corner cell. 
  * --> If true, adds 250. 
  * --> If false, goto next step.
* Checks to see if this is a critical cell. 
  * --> If true, adds 1000. Goto finish.
  * --> If false, goto next step.
* Add a random number to score between 1-500. Goto finish.


## Result

This beats the DemoAI about 25% of the time which is OK for a first attempt.


As a human player it is quite challenging to play because it jumps all over the board.


It makes the game more interesting for me anyway.
